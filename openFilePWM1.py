# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 11:26:26 2018
Extract the angular eigenfrequencies data from a file wher it was saved.
Plot the dispersion relations.

@author: utilisateur1
"""
import numpy as np
import os
import matplotlib.pyplot as plt

def openPWMresults(dossier, date, inclusion_type, nb_G, nb_mode =  -1):
    """ Cette fonction extrait les resultats d'un calcul par Plane Wave method.
        Elle lit un fichier compresse .gz, puis affiche les relations de
        dispersion et la densite d'etat."""
    # On determine le chemin vers le fichier
    file_path_name = dossier + "/"+date + "_MagCrystPWM_" + inclusion_type +\
                    str(nb_G) + ".gz"
    # On extrait l'information du fichier (un numpy array)
    data = np.loadtxt(file_path_name)
    # seule la derniere ligne ne contient pas de valeurs propres
    w_all_array = data[0:-1, :]
    # On classe les frequences pour pouvoir eliminer les artefacts plus
    # facilement
    w_all_array = np.sort(w_all_array, axis=1)
    #Une liste d'index identifiera certains artefacts
    index_list =  []
    for index in range(np.size(w_all_array, axis=1)): # parcours des modes
        # Si tous les w sont =0 dans une colonne, on l'identifie comme artefact
        if np.all(w_all_array[:,index] ==0.0)==True:
            index_list.append(index)
    # On elimine ces artefacts
    w_all_array = np.delete(w_all_array, index_list, axis=1)
    # La derniere ligne des data contient des infos sur le cristal magnonique
    # on y trouve les index des pts de haute symétrie dans la liste des
    # resultats, le nb total de G utilises et des 0 de remplissage       
    info = data[-1, :]
    # On elimine les zeros de remplissage
    info = np.delete(info, np.where(info==0))
    
    # On determine le nombre de modes a tracer/considerer si ce n'est fait
    nG_total = int(info[-1])
    nG_total_not0 = np.size(w_all_array, axis=1)
    if nb_mode == -1: #si l'utilisateur n'a rien specifie
        nb_mode = nG_total_not0 # on prend tous les modes
    
    # on identifie les freq ou k=0
    uniform = w_all_array[0,:nb_mode]
    
    """Plot the dispersion relations"""
    fig1, axes1 = plt.subplots(nrows=1, ncols=2, sharey=True)
    axes1[0].set_title(r"Relations de dispersion du cristal magnonique ",\
                        fontsize=20)
    for i in range(nb_mode):
        axes1[0].plot(w_all_array[:,i]*((1e-9)/(2.0*np.pi)), 'b.',\
                        markersize=1)
        axes1[0].plot(w_all_array[:,i]*((1e-9)/(2.0*np.pi)), 'b-',\
                        markersize=1, alpha=0.15)
        
    axes1[0].set_xlabel(r"vecteur d'onde $\vec{k}$", fontsize=16)
    axes1[0].set_ylabel(r"frequence $f$ $[GHz]$", fontsize=16)
    
    if len(info)==6: # info si reseau rectangulaire: [X,M,Gamma,X',M,nG_total]
        #points de haute symetrie pour reseau rectangulaire
        axes1[0].set_xticks([0,info[0],info[1],info[2], info[3], info[4] ])
        axes1[0].set_xticklabels([r"$\Gamma$",r"$X$",r"$M$",r"$\Gamma$",\
                                    r"$X'$",r"$M$" ], fontsize=16)
    else: 
        #points de haute symetrie pour reseau hexa
        axes1[0].set_xticks([0,info[0],info[1],info[2] ])
        axes1[0].set_xticklabels([r"$\Gamma$",r"$K$", r"$M$", r"$\Gamma$" ],\
                                    fontsize=16)
        
    # affiche le nombre d'ondes planes utilisees par le calcul                        
    axes1[0].text(0.0, w_all_array[0][0]*1e-9/(2*np.pi),\
            r"$\vec{H}_0$ in plane, nb de vecteurs $\vec{G}$: "+str(nG_total),\
                fontsize=12)
                
    """compute and plot the density of state"""
    def density_compute(sample, nb_section):
        sample = np.array(sample).flatten()
        wmin = np.min(sample)
        wmax = np.max(sample)
        w_range0, step = np.linspace(wmin, wmax, nb_section+1, endpoint=True,\
                                        retstep=True)
        w_range1 = w_range0 - step/2
        w_range = w_range1[1:]
        
        density = np.zeros(len(w_range))
        for w in sample:
            n = int(np.floor( (w - wmin)/step))
            if n == len(density):
                density[-1] += 1
            else:
                density[n] += 1
        density = density/np.sum(density)
        return density, w_range, sample
    
    nb_section = 50
    density_of_state, w_range, sample = density_compute(w_all_array[:,:nb_mode],\
                                                         nb_section)
    
    axes1[1].set_title(r"Densite d'etat pour chaque energie", fontsize=20)
    axes1[1].plot(density_of_state, w_range*((1e-9)/(2.0*np.pi)), 'r-',\
                    markersize=4)
    axes1[1].plot(np.zeros(len(uniform)), uniform*((1e-9)/(2.0*np.pi)), 'go')
    # On pourrait aussi utiliser un histogramme (plus exact sur la
     # representation, mais problemes de normalisation)
    #axes1[1].hist((w_all_array[:,:nb_mode]).flatten()*((1e-9)/(2.0*np.pi)),\
    #               bins=nb_section, alpha=0.45, normed=True,\
    #               orientation='horizontal')
    axes1[1].set_xlabel(r"densite d'etat normalisee $g(f)$", fontsize=16)
    # On ajuste les dimensions des figures
    fig1.subplots_adjust(hspace = 0.5, wspace = 0.3)
    fig1.set_size_inches(20, 20, forward=True)
    # On affiche les graphes
    fig1.tight_layout(w_pad = -0.0)
    plt.show()

"""extract the data from the file"""
dossier = os.getcwd() #dans le dossier actuel
date = "12-10-2019" #date de fin du calcul
# On indique le type d'inclusion et le nb d'ondes planes pour identifier
# le fichier a ouvrir
inclusion_type = "rectangle"# ou "cylindreIP" ou "cylindreOoP" ou "delta"
nb_G = 289

# nb de modes a tracer 
nb_mode = 25

# On execute la fonction (ici pour un seul fichier, par exemple)
openPWMresults(dossier, date, inclusion_type, nb_G, nb_mode)
