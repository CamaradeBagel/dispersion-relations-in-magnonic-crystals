This project was elaborated during summer 2018 while an internship in the
Laboratoire de Magnétisme of Polytechnique Montréal, and was written and
commented half in French in that context.
It's also a tad buggy, in the end solutions obtained with it are
somewhat not convergent.
The code find solution to a formulation of the Landau-Lifshitz equation in
periodic material, yielding the dispersion relation of magnons for magnonic
crystals using the plane waves expension method.
The definition of a magnonic crystal and domain to solve is general and modular,
and the solver is not perfectly optimized but makes use of parallel computing.
It's a bit hermetic, even if the computational part is quite simple, but I guess
I'll update it if I have some unexpected free time at some point, it was a fun 
project. Play with this if you want, it makes nice plots to pretend you're
a physicist.