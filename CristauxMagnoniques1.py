# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 16:28:55 2018

Definition des methodes de calcul des relations de dispersion d'ondes de spin de cristaux magnoniques.
Plane Wave Method pour des cristaux magnoniques sous forme d'inclusion dans une matrice.
résolution de Landau-Lifshitz avec transformees de Fourier et ondes de Bloch
_______________________________________________________________________________
version simple, les champs de demagnetisation en x/y, anisotropie sont
 "caches" dans un champ efficace.
 
 On considere un champs externe uniforme de saturation applique selon l'axe z.
 
On considere un reseau possiblement 3D,
 sans imposer de conditions frontières explicites.
 
On considère que le système varie legerement autour d'un etat a l'equilibre,
 ou le champ applique amene tout le système a saturation selon l'axe z.
Les variation sont consideres minimes.

***ici reseau est entendu au sens de super-reseau de propriete magnetiques,
 pas de reseau atomique***

les types de cristaux implémentés sont:
-des inclusions sous forme de prismes rectangulaires ('rectangles') dans une
 matrice selon une magnetisation In Plane (IP) ou Out Of Plane (OoP) par
 rapport au reseau
-des inclusions sous forme de cylindres dans une matrice selon une
 magnetisation In Plane (IP) ou Out Of Plane (OoP) par rapport au reseau

@author: Bastien Gauthier-Soumis
"""
import timeit
import numpy as np
import scipy.special as special
import scipy.linalg as linalg
import os
from multiprocessing import Pool
from  datetime import datetime

"""Definition de constantes physiques"""
mu0 = (4.0e-7)*np.pi #permeabilite du vide [T*m/A ]
gamma = 176e9# [rad/(s*T)] valeur absolue du rapport gyromagnetique electronique

"""On definie une classe pour contenir les information sur un cristal
   magnonique, les calculs s'effectueront comme des methodes de celle-ci"""
class MagnonicCrystal():
    """Classe representant un cristal magnonique constitue d'inclusions dans
        une matrice. Les informations pertinente sur une instance ( le reseau 
        reciproque fini decrivant approximativement sa structure, la forme des 
        inclusions, les caracteristiques magnetiques des materiaux le
        composant et sa description particuliere) sont enregistrees comme des
        proprietes de l'instance. Les transformees de Fourier des proprietes
        magnetiques sont definie comme des methodes de l'instance.
        Une methode est defini pour calculer les relations de dispersion sur
        un certain trajet dans le reseau reciproque, et pour un certain champ
        externe. Une autre calcule pour un seul vecteur d'onde, dans le but de
        paralleliser les calculs. Chacune de ces methodes existe en deux
        versions, l'une considerant le champs d'echange et l'autre non."""
    def __init__(self, G_data, Ms_r, lex2_r, inclusion):
        # initialisation des attributs de l'instance selon les arguments de la classe
        self.G_list = G_data[0] # liste des vecteurs du reseau reciproque 
        self.maille_param = G_data[1]#[m], parametre de maille, [a,t] si reseau hexagonal, [ax ou az,ay,t] si reseau 'rectangle'
        self.maille_volume = G_data[2]# [m^3], volume de la maille elementaire 
        self.inclusion = inclusion # [type,[forme]] des inclusions
        self.inclusion_type = inclusion[0] # type d'inclusion parmi 'cylindreIP', 'cylindreOoP', 'rectangle'
        self.inclusion_param = inclusion[1] # [rayon] si cylindre, [lx, ly] si rectangle, 1 si delta
        self.Ms_inclusion =  Ms_r[0] # [A/m], Magnetisation a saturation des inclusion
        self.Ms_matrice = Ms_r[1] # [A/m], Magnetisation a saturation de la matrice
        self.lex2_inclusion =  lex2_r[0] # [m**2], echange des inclusion
        self.lex2_matrice = lex2_r[1] # [m**2], echange de la matrice
        self.description = "Un cristal magnonique"
        # si inclusions cylindriques
        if self.inclusion_type == 'cylindreIP' or self.inclusion_type == 'cylindreOoP' :
            self.rayon = self.inclusion_param[0]
            self.t = self.maille_param[-1] # [m], epaisseur (thickness) de la couche mince
        # si inclusions prismes rectangulaires 
        elif self.inclusion_type == 'rectangle':
            self.lx, self.ly = self.inclusion_param[0], self.inclusion_param[1] #lx peut aussi vouloir signifier lz si in plane
            self.t = self.maille_param[-1] # [m], epaisseur (thickness) de la couche mince
        # si inclusions sont des points au centre d'une maille
        elif self.inclusion_type == 'delta':
            self.t = self.maille_param[-1] # [m], epaisseur (thickness) de la couche mince
        #si le type d'inclusion n'est pas reconnu
        else:
            self = None
            print("Erreur: type d'inclusion non reconnu")
    # Definition d'une fonction utilisee pour definir la transformee de Fourier
            #des proprietes magnetiques       
    # Il s'agit de la transformee d'une fonction qui prend la valeur 1 la ou il
            #y a des inclusions et 0 ailleurs
    def S(self, G):
        """ transformee de Fourier d'une fonction signe ayant la forme des
            inclusions d'un cristal magnonique"""
        if self.inclusion_type == 'cylindreIP': #si in plane
            Gx, Gy, Gz = G[0], G[1], G[2]
            F1 = (-self.t*2.0*np.pi*self.rayon**2)/self.maille_volume
            if Gy==0.0 and Gz==0.0:
                F2 = 0.5
            else:
                Gzy = np.sqrt(Gz**2+Gy**2)
                F2 = special.j1(self.rayon*Gzy)/(Gzy*self.rayon)
            F3 = np.sinc(Gx*self.t/2.0)
            return F1*F2*F3
        elif self.inclusion_type == 'cylindreOoP': #si out of plane
            Gx, Gy, Gz = G[0], G[1], G[2]
            F1 = (-self.t*2.0*np.pi*self.rayon**2)/self.maille_volume
            if Gx == 0.0 and Gy==0.0 :
                F2 = 0.5
            else:
                Gxy = np.sqrt(Gx**2+Gy**2)
                F2 = special.j1(self.rayon*Gxy)/(Gxy*self.rayon)
            F3 = np.sinc(Gz*self.t/2.0)
            return F1*F2*F3
        elif self.inclusion_type == 'rectangle':
            Gx, Gy, Gz = G[0], G[1], G[2]
            F1 = -(self.lx*self.ly*self.t)/(self.maille_volume)
            F2 = np.sinc(Gz*self.t/2.0)*np.sinc(Gx*self.lx/2.0)*np.sinc(Gy*self.ly/2.0)
            return F1*F2
        elif self.inclusion_type == 'delta':
            Gx, Gy, Gz = G[0], G[1], G[2]
            # transform of a dirac delta in R3 is a constant
            return 1./self.maille_volume

    # definition de la transformee elle-meme
    def Ms_G(self, G):
        """transformee de Fourier de la magnetisation a saturation d'un cristal
            magnonique"""
        if linalg.norm(G)==0:
            null_G = np.array([0,0,0])
            return self.Ms_matrice + (self.Ms_inclusion -\
                                        self.Ms_matrice)*self.S(null_G)
        else :
            return (self.Ms_inclusion - self.Ms_matrice)*self.S(G)
    def lex2_G(self, G):
        """transformee de Fourier de la longueur d'echange d'un cristal
            magnonique"""
        if linalg.norm(G)==0:
            null_G = np.array([0,0,0])
            return self.lex2_matrice + (self.lex2_inclusion -\
                                        self.lex2_matrice)*self.S(null_G)
        else :
            return (self.lex2_inclusion - self.lex2_matrice)*self.S(G)
            
    def PWM_matrix_Hex(self, H0, k):
        """La methode prend en entree un champ externe applique et un vecteur
        d'onde k et retourne la matrice associee au probleme de valeurs propres
        determine selon la PWM. Le champ d'echange est inclu."""
        H_eff = H0 #!!!! eventuellement, on pourrait developper
        
        # On a besoin du delta de kronecker
        def d_kronecker(i,j):
            if i == j:
                return 1.0
            else:
                return 0.0
                
        # Definition des blocs de la matrice dont on cherche les
        # valeurs propres, voir la section 1 (theorie)
        def Mxx_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx, ky = k[0], k[1]
            Gjx, Gjy = Gj[0], Gj[1]
            if linalg.norm(k + Gj) == 0.0:
                return 0.0
            else:
                F1a = self.Ms_G(Gi - Gj) 
                F1b = (kx + Gjx)*(ky + Gjy)/ (linalg.norm(k + Gj)**2)
                return -F1a*F1b
            
        def Myy_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx, ky = k[0], k[1]
            Gjx, Gjy = Gj[0], Gj[1]
            if linalg.norm(k + Gj) == 0.0:
                return 0.0
            else:
                F1a = self.Ms_G(Gi - Gj) 
                F1b = (kx + Gjx)*(ky + Gjy)/ (linalg.norm(k + Gj)**2)
                return F1a*F1b
        
        def Mxy_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            ky = k[1]
            Gjy = Gj[1]
            Giz = Gi[2]
            Gjz = Gj[2]
            F1 = d_kronecker(i,j)*H0#Heff_G(Gi-Gj)!!!!
            if linalg.norm(k + Gj) == 0.0:
                F2a = 0.0
                F2b = 0.0
            else:
                F2a = self.Ms_G(Gi-Gj)
                F2b = ((ky + Gjy)**2)/ (linalg.norm(k + Gj)**2)
            if linalg.norm(Gi -Gj) == 0.0:
                F3a = 0.0
                F3b = 0.0
            else:
                F3a = self.Ms_G(Gi -Gj)
                F3b = ((Giz - Gjz)**2 )/ (linalg.norm(Gi - Gj)**2)
            F4 = 0.0
            for Gl in self.G_list:
                f4a = self.lex2_G(Gl-Gj)
                f4b = np.dot((k+Gl) , (k+Gj))
                f4c = self.Ms_G(Gi-Gj)
                F4 = F4 + f4a*f4b*f4c
            return -F1 - F2a*F2b + F3a*F3b - F4
            
        def Myx_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx= k[0]
            Gjx = Gj[0]
            Giz = Gi[2]
            Gjz = Gj[2]
            F1 = d_kronecker(i,j)*H0#Heff_G(Gi-Gj)!!!!
            if linalg.norm(k + Gj) == 0.0:
                F2a = 0.0
                F2b = 0.0
            else:
                F2a = self.Ms_G(Gi-Gj) 
                F2b = ((kx + Gjx)**2)/ (linalg.norm(k + Gj)**2)
            if linalg.norm(Gi -Gj) == 0.0:
                F3a = 0.0
                F3b = 0.0
            else:
                F3a = self.Ms_G(Gi -Gj) 
                F3b = ((Giz - Gjz)**2)/ (linalg.norm(Gi - Gj)**2 )
            F4 = 0.0
            for Gl in self.G_list:
                f4a = self.lex2_G(Gl-Gj)
                f4b = np.dot((k+Gl) , (k+Gj))
                f4c = self.Ms_G(Gi-Gj)
                F4 = F4 + f4a*f4b*f4c
            return F1 + F2a*F2b - F3a*F3b + F4
            
        # On determine la taille de la matrice a resoudre, le nb d'onde plane
        NG = len(self.G_list)
        # On cre la matrice a resoudre pour ce k
        Mxx = np.empty((NG,NG)) # initialisation de np.array vides
        Mxy = np.empty((NG,NG))
        Myx = np.empty((NG,NG))
        Myy = np.empty((NG,NG))
        for i in range(NG):
            for j in range(NG):
                # On utilise les fonctions definies precedemment
                Mxx[i,j] = Mxx_ij(k, i,j) 
                Mxy[i,j] = Mxy_ij(k, i,j)
                Myx[i,j] = Myx_ij(k, i,j)
                Myy[i,j] = Myy_ij(k, i,j)
        row1 = np.hstack((Mxx,Mxy)) # combinaison des blocs
        row2 = np.hstack((Myx,Myy))
        Matrice_Hex = np.vstack((row1,row2)) # matrice finale
        return Matrice_Hex
        
    def PWM_matrix(self, H0, k):
        """La methode prend en entree un champ externe applique et un vecteur
        d'onde k et retourne la matrice associee au probleme de valeurs propres
        determine selon la PWM. Le champ d'echange n'est pas inclu."""
        H_eff = H0 #!!!! eventuellement, on pourrait developper
        
        # On a besoin du delta de kronecker
        def d_kronecker(i,j):
            if i == j:
                return 1.0
            else:
                return 0.0
                
        # Definition des blocs de la matrice dont on cherche les valeurs
        # propres, voir la section 1 (theorie)
        def Mxx_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx, ky = k[0], k[1]
            Gjx, Gjy = Gj[0], Gj[1]
            if linalg.norm(k + Gj) == 0.0:
                return 0.0
            else:
                F1a = self.Ms_G(Gi - Gj) 
                F1b = (kx + Gjx)*(ky + Gjy)/ (linalg.norm(k + Gj)**2)
                return -F1a*F1b
            
        def Myy_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx, ky = k[0], k[1]
            Gjx, Gjy = Gj[0], Gj[1]
            if linalg.norm(k + Gj) == 0.0:
                return 0.0
            else:
                F1a = self.Ms_G(Gi - Gj) 
                F1b = (kx + Gjx)*(ky + Gjy)/ (linalg.norm(k + Gj)**2)
                return F1a*F1b
        
        def Mxy_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            ky = k[1]
            Gjy = Gj[1]
            Giz = Gi[2]
            Gjz = Gj[2]
            F1 = d_kronecker(i,j)*H0#Heff_G(Gi-Gj)!!!!
            if linalg.norm(k + Gj) == 0.0:
                F2a = 0.0
                F2b = 0.0
            else:
                F2a = self.Ms_G(Gi-Gj)
                F2b = ((ky + Gjy)**2)/ (linalg.norm(k + Gj)**2)
            if linalg.norm(Gi -Gj) == 0.0:
                F3a = 0.0
                F3b = 0.0
            else:
                F3a = self.Ms_G(Gi -Gj)
                F3b = ((Giz - Gjz)**2 )/ (linalg.norm(Gi - Gj)**2)
            return -F1 - F2a*F2b + F3a*F3b
            
        def Myx_ij(k, i,j):
            Gi = self.G_list[i]
            Gj = self.G_list[j]
            kx= k[0]
            Gjx = Gj[0]
            Giz = Gi[2]
            Gjz = Gj[2]
            F1 = d_kronecker(i,j)*H0#Heff_G(Gi-Gj)!!!!
            if linalg.norm(k + Gj) == 0.0:
                F2a = 0.0
                F2b = 0.0
            else:
                F2a = self.Ms_G(Gi-Gj) 
                F2b = ((kx + Gjx)**2)/ (linalg.norm(k + Gj)**2)
            if linalg.norm(Gi -Gj) == 0.0:
                F3a = 0.0
                F3b = 0.0
            else:
                F3a = self.Ms_G(Gi -Gj) 
                F3b = ((Giz - Gjz)**2)/ (linalg.norm(Gi - Gj)**2 )
            return F1 + F2a*F2b - F3a*F3b
            
        # On determine la taille de la matrice a resoudre, le nb d'onde plane
        NG = len(self.G_list)
        # On cre la matrice a resoudre pour ce k
        Mxx = np.empty((NG,NG)) # initialisation de np.array vides
        Mxy = np.empty((NG,NG))
        Myx = np.empty((NG,NG))
        Myy = np.empty((NG,NG))
        for i in range(NG):
            for j in range(NG):
                # On utilise les fonctions definies precedemment
                Mxx[i,j] = Mxx_ij(k, i,j) 
                Mxy[i,j] = Mxy_ij(k, i,j)
                Myx[i,j] = Myx_ij(k, i,j)
                Myy[i,j] = Myy_ij(k, i,j)
        row1 = np.hstack((Mxx,Mxy)) # combinaison des blocs
        row2 = np.hstack((Myx,Myy))
        Matrice = np.vstack((row1,row2)) # matrice finale
        return Matrice

    """ Dans la classe, une methode calcule les relations de dispersion avec la PWM"""
    def PWM_compute(self,H0, k_list):
        """ Calcule les relations de dispersion sur un trajet de vecteurs
        d'ondes dans l'espace reciproque selon la Plane Wave decomposition 
        Method. Il s'agit de construire une matrice pour chaque vecteur 
        d'onde k sur le trajet et de trouver ses valeurs propres pour
        identifier les fréquences des modes a ce k.
        Le calcul n'est pas parallele. N'inclue pas le champ d'echange
        Prend en entree le champ externe applique (amplitude) et la liste de k 
        a considerer. retourne les vecteurs propres des matrices, les valeurs
        propres des matrices (voir la doc de scipy.linalg.eigvals() ),
        les frequences angulaires prorpes associees, les frequences prorpes
        associees, la liste des matrices considerees et la duree du calcul."""
        t0 = timeit.default_timer() # temps initial
        H_eff = H0 #!!!! eventuellement, on pourrait developper
        
        # initialisation de listes qui contiendront les liste de valeurs/vecteurs propres
        iOmega_all = [] #valeurs propres, pour tous les k
        w_all =  [] #freq angulaires propres, pour tous les k
        f_all = [] #freq propres, pour tous les k
        vec_m_all = [] #vecteurs propres, magnetisation dynamique, pour tous les k
        # Pour chaque k sur le chemin
        for k in k_list:             
            Matrice = self.PWM_matrix(H0, k) # matrice definie avec PWM
            # On trouve les valeurs et vecteurs propres de la Matrice
            # ils sont sauvegardés dans une liste et une liste de vecteurs
            iOmega_list, vec_m_list = linalg.eig(Matrice)   
            # On transforme en np.array pour simplifier les manipulations
            iOmega_array = np.array(iOmega_list)
            # On calcule les frequences et freq angulaires #!!!!
            w_array = (mu0*gamma/(1.0j))*iOmega_array # [rad/s],freq angulaires
            w_array = np.abs(w_array.real) # on garde la partie reelle
            f_array = w_array / (2.0*np.pi) # [Hz], frequence   
            # On ajoute les liste de ce k à toutes les listes
            iOmega_all.append(iOmega_array)
            w_all.append(w_array)
            f_all.append(f_array)
            vec_m_all.append(vec_m_list) 
            time = timeit.default_timer()-t0 # temps du calcul
        #On retourne les donnees et la derniere matrice utilisee
        return vec_m_all, iOmega_all, w_all, f_all, Matrice, time 
        
    def PWM_compute_Hex(self,H0, k_list):
        """ Calcule les relations de dispersion sur un trajet de vecteurs
        d'ondes dans l'espace reciproque selon la Plane Wave decomposition 
        Method. Il s'agit de construire une matrice pour chaque vecteur 
        d'onde k sur le trajet et de trouver ses valeurs propres pour
        identifier les fréquences des modes a ce k.
        Le calcul n'est pas parallele. Inclue le champ d'echange.
        Prend en entree le champ externe applique (amplitude) et la liste de k 
        a considerer. retourne les vecteurs propres des matrices, les valeurs
        propres des matrices (voir la doc de scipy.linalg.eigvals() ),
        les frequences angulaires prorpes associees, les frequences prorpes
        associees, la liste des matrices considerees et la duree du calcul."""
        t0 = timeit.default_timer() # temps initial
        
        # initialisation de listes qui contiendront les liste de
        # valeurs/vecteurs propres
        iOmega_all = [] #valeurs propres, pour tous les k
        w_all =  [] #freq angulaires propres, pour tous les k
        f_all = [] #freq propres, pour tous les k
        vec_m_all = [] #vecteurs propres, magnetisation dynamique, pour tous les k
        # Pour chaque k sur le chemin
        for k in k_list:           
            Matrice_Hex = self.PWM_matrix_Hex(H0, k) # matrice definie avec PWM
            # On trouve les valeurs et vecteurs propres de la Matrice
            # ils sont sauvegardés dans une liste et une liste de vecteurs
            iOmega_list, vec_m_list = linalg.eig(Matrice_Hex)   
            # On transforme en np.array pour simplifier les manipulations
            iOmega_array = np.array(iOmega_list)
            # On calcule les frequences et freq angulaires #!!!!
            w_array = (mu0*gamma/(1.0j))*iOmega_array # [rad/s],freq angulaires
            w_array = np.abs(w_array.real) # on garde la partie reelle
            f_array = w_array / (2.0*np.pi) # [Hz], frequence   
            # On ajoute les liste de ce k à toutes les listes
            iOmega_all.append(iOmega_array)
            w_all.append(w_array)
            f_all.append(f_array)
            vec_m_all.append(vec_m_list) 
            time = timeit.default_timer()-t0 # temps du calcul
        #On retourne les donnees et la derniere matrice utilisee
        return vec_m_all, iOmega_all, w_all, f_all, Matrice_Hex, time
    
    # Une autre fonction, legerement differente, retourne seulement les
    # frequences angulaires depuis  les  valeurs propres, et calcule seulement
    # pour un k
    def PWM_compute_parallel(self,H0, k):
        """ Calcule les relations de dispersion spour un vecteur
        d'ondek dans l'espace reciproque selon la Plane Wave decomposition 
        Method. Il s'agit de construire une matrice pour ce vecteur 
        d'onde k et de trouver ses valeurs propres pour identifier les
        fréquences des modes a ce k. Le calcul est pas parallelisable, et en
        combinant plusieurs execution parallele de cette methode, on peut 
        calculer les relations de dispersion sur un trajet de k 
        de maniere parallele.  N'inclue pas le champ d'echange."""
        print('process id:', os.getpid(), '-Start')
        
        Matrice = self.PWM_matrix(H0, k) # matrice definie avec PWM
        # On trouve les valeurs propres de la Matrice
        # ils sont sauvegardés dans une liste
        iOmega_list = linalg.eigvals(Matrice)   
        # On transforme en np.array pour simplifier les manipulations
        iOmega_array = np.array(iOmega_list)
        # On calcule les frequences angulaires
        w_array = (mu0*gamma/(1.0j))*iOmega_array # [rad/s], freq angulaires
        w_array = np.abs(w_array.real) # on garde la partie reelle
        #f_array = w_array / (2.0*np.pi) # [Hz], frequence
        print('process id:', os.getpid(), '-Finished')
        return w_array
        
    # ici on inclu aussi le champ d'echange (plus de ressources necessaires)
    def PWM_compute_parallel_Hex(self,H0, k):
        """ Calcule les relations de dispersion spour un vecteur
        d'ondek dans l'espace reciproque selon la Plane Wave decomposition 
        Method. Il s'agit de construire une matrice pour ce vecteur 
        d'onde k et de trouver ses valeurs propres pour identifier les
        fréquences des modes a ce k. Le calcul est pas parallelisable, et en
        combinant plusieurs execution parallele de cette methode, on peut 
        calculer les relations de dispersion sur un trajet de k 
        de maniere parallele. Inclue le champ d'echange."""
        print('process id:', os.getpid(), '-Start')
        Matrice_Hex = self.PWM_matrix_Hex(H0, k) # matrice definie avec PWM
        # On trouve les valeurs propres de la Matrice
        # ils sont sauvegardés dans une liste
        iOmega_list = linalg.eigvals(Matrice_Hex)   
        # On transforme en np.array pour simplifier les manipulations
        iOmega_array = np.array(iOmega_list)
        # On calcule les frequences angulaires
        w_array = (mu0*gamma/(1.0j))*iOmega_array # [rad/s], freq angulaires
        w_array = np.abs(w_array.real) # on garde la partie reelle
        #f_array = w_array / (2.0*np.pi) # [Hz], frequence
        print('process id:', os.getpid(), '-Finished')
        return w_array

"""Calcul parallelise des relations de dispersion"""
def ParaCompute(MagCryst, k_list, Nk_list, H0):
    """Cette fonction cre un ensemble de processus de calcul paralleles (pool)
        et lance un calcul de PWM pour un vecteur d'onde k sur chacun d'entre
        eux pour toute une liste de k. Elle sauvegarde ensuite les resultats
        dans un fichier .gz, affiche le temps de processeur utilise et retourne
        1.les valeurs propres trouvees dans un array Mx2N ou N est le nb 
          d'ondes planes utilises et M le nb de k considere (la ie ligne
          correspond au ie k sur le trajet)
        2. la liste de toutes les matrices dont on a trouve les valeurs propres
          (une par k)
        La fonction prend en entree le cristal magnonique a considerer, la
        liste de vecteur d'onde ou evaluer les relations de dispersion,
        la liste contenant les position des points de haute symetrie dans la
        precedente liste et le champ externe applique.
        Le champ d'echange n'est pas inclu."""
    # ou MagCryst est une instance de MagnonicCrystal
    # On verifie qu'on execute le programme directement, pour eviter les bugs
    print('computation begin')
    if __name__=='__main__':
        print('main')
    else:
        # On defini l'instant initial
        t0 = timeit.default_timer()
        # On cre un pool de processus en utilisant tous les coeurs disponibles
        pool = Pool(processes=3) #defini depuis multiprocessing
        # On appelle l'execution du calcul pour chaque vec d'onde k de maniere
        #   parallele a partir du pool
        results = [pool.apply_async(MagCryst.PWM_compute_parallel,\
                                    (H0, k)) for k in k_list]
        # On recupere les resultats de chaque processus, et on mets 
        #   l'ensembles de ceux-ci (en ordre car on a utilise un pool) dans un
        #   numpy.array
        w_all_array = np.array([res.get() for res in results])
        pool.terminate()
        pool.join()
        # On estime la duree du calcul selon le temps du processeur
        t1 = timeit.default_timer() - t0
        """Sauvegarde des resultats dans un fichier texte"""
        # On ajoute quelques infos utiles pour l'affichage ulterieur des resultats
        nG_total = len(MagCryst.G_list) # nb total d'ondes planes considerees
        if len(Nk_list)==5: # si reseau rectangulaire
            # On sauvegarde les positions des points de hautes symetrie
            #   dans la liste de k
            info = np.array([Nk_list[0],Nk_list[1],Nk_list[2],Nk_list[3],\
                            Nk_list[4], nG_total])
        if len(Nk_list)==3: # si reseau hexagonal
            info = np.array([Nk_list[0],Nk_list[1],Nk_list[2], nG_total])
        # ajoute des zeros pour completer la ligne
        remplissage = np.zeros(2*nG_total-len(info))
        info = np.hstack((info, remplissage))
        # creation d'un seul array pour toute l'information
        data=  np.vstack((w_all_array, info))
        # On determine le directory actuel
        dossier = os.getcwd()
        # On defini le nom et chemin du fichier
        file_path_name = dossier + "/"+datetime.now().strftime("%d-%m-%Y") +\
                            "_MagCrystPWM_"+str(MagCryst.inclusion_type)+str(nG_total)+".gz"
        # Sauvegarde de l'information        
        np.savetxt(file_path_name, data,\
                header="Magnon angular eigenfrequencies obtained by PWM \n"+\
                str(MagCryst.inclusion_type) )
        # On estime la duree de l'ecriture selon le temps du processeur
        t2 = timeit.time.clock() - t1        
        print("Le calcul a dure ", t1, " secondes")
        print("L'ecriture a dure ", t2, " secondes")        
        return w_all_array, info
        
def ParaCompute_Hex(MagCryst, k_list, Nk_list, H0):
    """Cette fonction cre un ensemble de processus de calcul paralleles (pool)
        et lance un calcul de PWM pour un vecteur d'onde k sur chacun d'entre
        eux pour toute une liste de k. Elle sauvegarde ensuite les resultats
        dans un fichier .gz, affiche le temps de processeur utilise et retourne
        1.les valeurs propres trouvees dans un array Mx2N ou N est le nb 
          d'ondes planes utilises et M le nb de k considere (la ie ligne
          correspond au ie k sur le trajet)
        2. la liste de toutes les matrices dont on a trouve les valeurs propres
          (une par k)
        La fonction prend en entree le cristal magnonique a considerer, la
        liste de vecteur d'onde ou evaluer les relations de dispersion,
        la liste contenant les position des points de haute symetrie dans la
        precedente liste et le champ externe applique.
        Le champ d'echange est inclu."""
    # ou MagCryst est une instance de MagnonicCrystal
    print('computation begin')
    # On verifie qu'on execute le programme directement, pour eviter les bugs
    if __name__=='__main__':
        print('main')
    else:
        # On defini l'instant initial
        t0 = timeit.default_timer()
        # On cre un pool de processus en utilisant tous les coeurs disponibles
        pool = Pool(processes=3) #defini depuis multiprocessing
        # On appelle l'execution du calcul pour chaque vec d'onde k de maniere
        #   parallele a partir du pool
        results = [pool.apply_async(MagCryst.PWM_compute_parallel_Hex,\
                                    (H0, k)) for k in k_list]
        # On recupere les resultats de chaque processus, et on mets 
        #   l'ensembles de ceux-ci (en ordre car on a utilise un pool) dans un
        #   numpy.array
        w_all_array = np.array([res.get() for res in results])
        pool.terminate()
        pool.join()
        # On estime la duree du calcul selon le temps du processeur
        t1 = timeit.default_timer() - t0
        """Sauvegarde des resultats dans un fichier texte"""
        # On ajoute quelques infos utiles pour l'affichage ulterieur des resultats
        nG_total = len(MagCryst.G_list) # nb total d'ondes planes considerees
        if len(Nk_list)==5: # si reseau rectangulaire
            # On sauvegarde les positions des points de hautes symetrie
            #   dans la liste de k
            info = np.array([Nk_list[0],Nk_list[1],Nk_list[2],Nk_list[3],\
                            Nk_list[4], nG_total])
        if len(Nk_list)==3: # si reseau hexagonal
            info = np.array([Nk_list[0],Nk_list[1],Nk_list[2], nG_total])
        # ajoute des zeros pour completer la ligne
        remplissage = np.zeros(2*nG_total-len(info))
        info = np.hstack((info, remplissage))
        # creation d'un seul array pour toute l'information
        data=  np.vstack((w_all_array, info))
        # On determine le directory actuel
        dossier = os.getcwd()
        # On defini le nom et chemin du fichier
        file_path_name = dossier + "/"+datetime.now().strftime("%d-%m-%Y") +\
                            "_MagCrystPWM_"+str(MagCryst.inclusion_type)+"Hex"+str(nG_total)+".gz"
        # Sauvegarde de l'information        
        np.savetxt(file_path_name, data,\
                header="Magnon angular eigenfrequencies obtained by PWM \n"+\
                str(MagCryst.inclusion_type) )
        # On estime la duree de l'ecriture selon le temps du processeur
        t2 = timeit.time.clock() - t1        
        print("Le calcul a dure ", t1, " secondes")
        print("L'ecriture a dure ", t2, " secondes")        
        return w_all_array, info
        
""" Definition de fonctions pour creer des reseaux dans l'espace reciproque"""
def create_rectangular_lattice_reciproqual_IP(nG, az, ay, t):
    """Creation d'un reseau de cristal magnonique ou la structure est
    rectangulaire et In Plane par rapport au xhamp externe (donc dans le plan 
    y-z). Prend en entree le nb nG de rang de voisins depuis l'origine a
    definir, les parametres de maille az et ay et l'epaisseur t du cristal.
    retourne un array contenant la liste des vecteurs G du reseau, les
    parametres de maille et le volume d'une maille elementaire."""
    # On a les vecteurs de base du reseau direct, a1 et a2
    # a1 = (0,0,az)  a2 = (0,ay,0)
    # On a donc les vecteurs de base du reseau reciproque, b1 et b2
    b1x = 0.0
    b1y = 0.0
    b1z = 2.0*np.pi/az #in plane
    b1 = np.array(([b1x,b1y,b1z]))
    b2x = 0.0
    b2y = 2.0*np.pi/ay
    b2z = 0.0
    b2 = np.array(([b2x,b2y,b2z]))
    # On cre un ensemble de vecteurs du reseau reciproque symetrique pour
    # la sommation des ondes planes
    G_list = []# liste initialement vide
    for n1 in np.arange(-nG, nG+1):
        for n2 in np.arange(-nG, nG+1):
            G_vec = n1*b1 + n2*b2
            G_list.append(G_vec)
    # On sauvegarde les param de maille afin de pouvoir les recuperer plus tard
    maille_param = [az, ay, t]
    # On calcule le volume d'une maille elementaire
    maille_volume = az*ay*t
    # On cre une liste avec toutes les informations utiles plus tard
    G_data = [G_list, maille_param, maille_volume]
    return G_data
    
def create_rectangular_lattice_reciproqual_OoP(nG, ax, ay, t):
    """Creation d'un reseau reciproque de cristal magnonique ou la structure
    est rectangulaire et Out of Plane par rapport au xhamp externe (donc dans
    le plan x-y). Prend en entree le nb nG de rang de voisins depuis l'origine
    a definir, les parametres de maille az et ay et l'epaisseur t du cristal.
    retourne un array contenant la liste des vecteurs G du reseau, les
    parametres de maille et le volume d'une maille elementaire."""
    # On a les vecteurs de base du reseau direct, a1 et a2
    # a1 = (ax,0,0)  a2 = (0,ay,0)
    # On a donc les vecteurs de base du reseau reciproque, b1 et b2
    b1x = 2.0*np.pi/ax # Out of Plane
    b1y = 0.0
    b1z = 0.0
    b1 = np.array(([b1x,b1y,b1z]))
    b2x = 0.0
    b2y = 2.0*np.pi/ay
    b2z = 0.0
    b2 = np.array(([b2x,b2y,b2z]))
    # On cre un ensemble de vecteurs du reseau reciproque symetrique pour
    # la sommation des ondes planes
    G_list = []# liste initialement vide
    for n1 in np.arange(-nG, nG+1):
        for n2 in np.arange(-nG, nG+1):
            G_vec = n1*b1 + n2*b2
            G_list.append(G_vec)
    # On sauvegarde les param de maille afin de pouvoir les recuperer plus tard
    maille_param = [ax, ay, t]
    # On calcule le volume d'une maille elementaire
    maille_volume = ax*ay*t
    # On cre une liste avec toutes les informations utiles plus tard
    G_data = [G_list, maille_param, maille_volume]
    return G_data
    
def create_hexagonal_lattice_reciproqual_OoP(nG, a, t):
    """Creation d'un reseau reciproque de cristal magnonique ou la structure
    est hexagonale et Out of Plane par rapport au xhamp externe (donc dans
    le plan x-y). Prend en entree le nb nG de rang de voisins depuis l'origine
    a definir, les parametres de maille az et ay et l'epaisseur t du cristal.
    retourne un array contenant la liste des vecteurs G du reseau, les
    parametres de maille et le volume d'une maille elementaire."""
    # vecteurs de base du reseau reciproque
    b1x = (2*np.pi/a) # Out of Plane
    b1y = -(2*np.pi/(a*np.sqrt(3)))
    b1z = 0.0
    b1 = np.array([b1x, b1y, b1z])  
    b2x = 0.0
    b2y = (4*np.pi)/(a*np.sqrt(3))
    b2z = 0.0
    b2 = np.array([b2x, b2y, b2z])
    # On cre une liste, l'ensemble de vecteurs du reseau reciproque pour la
    # sommation des ondes planes
    G_list = []# liste initialement vide
    # si G = l1*b1 + l2*b2, et que l'on cherche les nG rangs de voisins pour 
    # une maille hexagonale:
    # l2 un entier entre -nG et nG (0 inclu)
    # l1 un entier entre |l2|/l2 * (|l2|-nG) et |l2|/l2 *nG
    liste_iteration2 = np.arange(-nG, nG+1)
    for count2 in liste_iteration2:
        if count2 == 0:
            liste_iteration1 = np.arange(-nG, nG+1)
        else: 
            liste_iteration1 = np.sign(count2)*np.arange(np.abs(count2)-nG, nG+1)
        for count1 in liste_iteration1:
            G_vec = count2*b2 + count1*b1
            G_list.append(G_vec) 
    # On sauvegarde les param de maille afin de pouvoir les recuperer plus tard
    maille_param = [a, t]
    # On calcule le volume d'une maille elementaire
    maille_volume = (np.sqrt(3.0)*a**2*t)/2.0
    # On cre une liste avec toutes les informations utiles plus tard
    G_data = [G_list, maille_param, maille_volume]
    return G_data
    
def create_hexagonal_lattice_reciproqual_IP(nG, a, t):
    """Creation d'un reseau de cristal magnonique ou la structure est
    hexagonale et In Plane par rapport au xhamp externe (donc dans le plan 
    y-z). Prend en entree le nb nG de rang de voisins depuis l'origine a
    definir, les parametres de maille az et ay et l'epaisseur t du cristal.
    retourne un array contenant la liste des vecteurs G du reseau, les
    parametres de maille et le volume d'une maille elementaire."""
    # vecteurs de base du reseau reciproque
    b1x = 0.0
    b1y = -(2*np.pi/(a*np.sqrt(3)))
    b1z = (2*np.pi/a) # In Plane
    b1 = np.array([b1x, b1y, b1z])  
    b2x = 0.0
    b2y = (4*np.pi)/(a*np.sqrt(3))
    b2z = 0.0
    b2 = np.array([b2x, b2y, b2z])
    # On cre une liste, l'ensemble de vecteurs du reseau reciproque pour
    # la sommation des ondes planes
    G_list = []# liste initialement vide
    # si G = l1*b1 + l2*b2, et que l'on cherche les nG rangs de voisins pour
    # une maille hexagonale:
    # l2 un entier entre -nG et nG (0 inclu)
    # l1 un entier entre |l2|/l2 * (|l2|-nG) et |l2|/l2 *nG
    liste_iteration2 = np.arange(-nG, nG+1)
    for count2 in liste_iteration2:
        if count2 == 0:
            liste_iteration1 = np.arange(-nG, nG+1)
        else: 
            liste_iteration1 = np.sign(count2)*np.arange(np.abs(count2)-nG, nG+1)
        for count1 in liste_iteration1:
            G_vec = count2*b2 + count1*b1
            G_list.append(G_vec) 
    # On sauvegarde les param de maille afin de pouvoir les recuperer plus tard
    maille_param = [a, t]
    # On calcule le volume d'une maille elementaire
    maille_volume = (np.sqrt(3.0)*a**2*t)/2.0
    # On cre une liste avec toutes les informations utiles plus tard
    G_data = [G_list, maille_param, maille_volume]
    return G_data

def create_k_list_GammaXMGammaX1M_OoP(Nk, maille_param): # pour reseau 'rectangulaire' champ out of plane
    """ Creation d'un trajet de vecteurs d'onde k dans l'espace reciproque. Le
     trajet passe par les points de hautes symetrie Gamma,X,M,Gamma,X',M
     du reseau reciproque d'un cristal de structure rectangulaire selon le plan
     x-y (Out of plane). La fonction prend en entree le nombre approximatif de
     k a utiliser (arrondi vers le bas) et les parametres de maille du cristal.
     Elle retourne un array conteannt les k du trajet et une liste contenant
     les indices de la liste ou se trouve chaque points de hautes
     symetrie dans le array precedent, dans ordre indique ci-haut. """    
    #definition des pts d'interet    
    ax, ay = maille_param[0], maille_param[1]# parametres de maille
    Ax = 2.0*np.pi/(2.0*ax)
    Ay = 2.0*np.pi/(2.0*ay)
    Gammax = 0.001 # pas 0 pour eviter les erreurs numeriques dans la PWM
    Gammay = 0.001
    Xx = Ax
    Xy = 0.0
    X1x = 0.0
    X1y = Ay
    Mx = Ax
    My = Ay
    L_trajet_total = 2*Ax + 2*Ay + np.sqrt(Ax**2 + Ay**2)
    Nk1 = np.floor((Ax/L_trajet_total)*Nk) # proportion du trajet entre Gamma et X' ou X et M
    Nk2 = np.floor((Ay/L_trajet_total)*Nk) # proportion du trajet entre Gamma et X ou X' et M
    Nk3 = np.floor((np.sqrt(Ax**2 + Ay**2)/L_trajet_total)*Nk) # proportion du trajet entre Gamma et M
    # On determine le nombre de vecteurs reelement crees    
    Nk_list = [Nk1, Nk1+ Nk2, Nk1+ Nk2+ Nk3, Nk1+ 2*Nk2+ Nk3, 2*Nk1+ 2*Nk2+ Nk3]    
    # On cre une liste en se deplacant regulierement entre les pts d'interet
    k_list = [] #liste totale initialement vide
    # On cre les coordonnees sur les segments du trajet avec des arrays reguliers
    liste_kx1 = np.linspace(Gammax, Xx, Nk1, endpoint= False)
    liste_ky1 = np.linspace(Gammay, Xy, Nk1, endpoint= False)
    liste_kx2 = np.linspace(Xx, Mx, Nk2, endpoint= False)
    liste_ky2 = np.linspace(Xy, My, Nk2, endpoint= False)
    liste_kx3 = np.linspace(Mx, Gammax, Nk3, endpoint= False)
    liste_ky3 = np.linspace(My, Gammay, Nk3, endpoint= False)
    liste_kx4 = np.linspace(Gammax, X1x, Nk2, endpoint= False)
    liste_ky4 = np.linspace(Gammay, X1y, Nk2, endpoint= False)
    liste_kx5 = np.linspace(X1x, Mx, Nk1, endpoint= False)
    liste_ky5 = np.linspace(X1y, My, Nk1, endpoint= False)
    # on combine les listes de coordonnees
    liste_kx = np.hstack((liste_kx1,liste_kx2,liste_kx3,liste_kx4,liste_kx5))
    liste_ky = np.hstack((liste_ky1,liste_ky2,liste_ky3,liste_ky4,liste_ky5))
    # On utilise les listes de coordonnees pour creer les vecteurs d'ondes sur le trajet
    for i in range(len(liste_kx)):
        kx = liste_kx[i]
        ky = liste_ky[i]
        k = np.array([kx, ky,0.0])# out of plane
        k_list.append(k)
    return k_list, Nk_list  

def create_k_list_GammaXMGammaX1M_IP(Nk, maille_param): # pour reseau 'rectangulaire' champ in plane
    """ Creation d'un trajet de vecteurs d'onde k dans l'espace reciproque. Le
     trajet passe par les points de hautes symetrie Gamma,X,M,Gamma,X',M
     du reseau reciproque d'un cristal de structure rectangulaire selon le plan
     y-z (In plane). La fonction prend en entree le nombre approximatif de k a
     utiliser (arrondi vers le bas) et les parametres de maille du cristal.
     Elle retourne un array conteannt les k du trajet et une liste contenant
     les indices de la liste ou se trouve chaque points de hautes
     symetrie dans le array precedent, dans ordre indique ci-haut. """    
    #definition des pts d'interet    
    az, ay = maille_param[0], maille_param[1]# parametres de maille
    Az = 2.0*np.pi/(2.0*az)
    Ay = 2.0*np.pi/(2.0*ay)
    Gammaz = 0.001 # pas 0 pour eviter les erreurs numeriques lors de la PWM
    Gammay = 0.001
    Xz = Az
    Xy = 0.0
    X1z = 0.0
    X1y = Ay
    Mz = Az
    My = Ay
    L_trajet_total = 2*Az + 2*Ay + np.sqrt(Az**2 + Ay**2)
    Nk1 = np.floor((Az/L_trajet_total)*Nk) # proportion du trajet entre Gamma et X' ou X et M
    Nk2 = np.floor((Ay/L_trajet_total)*Nk) # proportion du trajet entre Gamma et X ou X' et M
    Nk3 = np.floor((np.sqrt(Az**2 + Ay**2)/L_trajet_total)*Nk) # proportion du trajet entre Gamma et M
    # On determine le nombre de vecteurs reelement crees
    Nk_list = [Nk1, Nk1+ Nk2, Nk1+ Nk2+ Nk3, Nk1+ 2*Nk2+ Nk3, 2*Nk1+ 2*Nk2+ Nk3]   
    # On cre une liste en se deplacant regulierement entre les pts d'interet
    k_list = [] #liste totale initialement vide
    # On cre les coordonnees sur les segments du trajet avec des arrays reguliers
    liste_kz1 = np.linspace(Gammaz, Xz, Nk1, endpoint= False)
    liste_ky1 = np.linspace(Gammay, Xy, Nk1, endpoint= False)
    liste_kz2 = np.linspace(Xz, Mz, Nk2, endpoint= False)
    liste_ky2 = np.linspace(Xy, My, Nk2, endpoint= False)
    liste_kz3 = np.linspace(Mz, Gammaz, Nk3, endpoint= False)
    liste_ky3 = np.linspace(My, Gammay, Nk3, endpoint= False)
    liste_kz4 = np.linspace(Gammaz, X1z, Nk2, endpoint= False)
    liste_ky4 = np.linspace(Gammay, X1y, Nk2, endpoint= False)
    liste_kz5 = np.linspace(X1z, Mz, Nk1, endpoint= False)
    liste_ky5 = np.linspace(X1y, My, Nk1, endpoint= False)
    # on combine les listes de coordonnees
    liste_kz = np.hstack((liste_kz1,liste_kz2,liste_kz3,liste_kz4,liste_kz5))
    liste_ky = np.hstack((liste_ky1,liste_ky2,liste_ky3,liste_ky4,liste_ky5))
    # On utilise les listes de coordonnees pour creer les vecteurs d'ondes sur le trajet
    for i in range(len(liste_kz)):
        kz = liste_kz[i]
        ky = liste_ky[i]
        k = np.array([0.0, ky,kz])# in plane
        k_list.append(k)
    return k_list, Nk_list

def create_k_list_GammaKMGamma_OoP(Nk, maille_param): # pour reseau 'hexagonal' champ out of plane
    """ Creation d'un trajet de vecteurs d'onde k dans l'espace reciproque. Le
     trajet passe par les points de hautes symetrie Gamma,K,M,Gamma
     du reseau reciproque d'un cristal de structure hexagonale selon le plan
     x-y (Out of plane). La fonction prend en entree le nombre approximatif de
     k a utiliser (arrondi vers le bas) et les parametres de maille du cristal.
     Elle retourne un array conteannt les k du trajet et une liste contenant
     les indices de la liste ou se trouve chaque points de hautes
     symetrie dans le array precedent, dans ordre indique ci-haut. """    
    #definition des pts d'interet    
    a = maille_param[0] # parametre de maille
    Gammax = 0.001 #eviter les erreurs numeriques lors de la PWM, out of plane
    Gammay = 0.001
    Kx = np.pi/a
    Ky = 0.0
    Mx = (np.sqrt(3.0)*np.pi)/(2.0*a)
    My = np.pi/(2.0*a)
    L_trajet_total = (Kx - Gammax) + np.sqrt((Mx - Kx)**2 +(My - Ky)**2) + np.sqrt((Gammax - Mx)**2 +(Gammay - My)**2)
    Nk1 = np.floor(((Kx - Gammax)/L_trajet_total)*Nk) # proportion du trajet, en points, entre Gamma et K
    Nk2 = np.floor((np.sqrt((Mx - Kx)**2 +(My - Ky)**2)/L_trajet_total)*Nk) # proportion du trajet, en points, entre K et M
    Nk3 = np.floor((np.sqrt((Gammax - Mx)**2 +(Gammay - My)**2)/L_trajet_total)*Nk) # proportion du trajet, en points, entre K et M
    Nk_list = [Nk1, Nk1+Nk2, Nk1+Nk2+Nk3] #nombre total de vecteur rellement crees    
    # On cre une liste en se deplacant regulierement entre les pts d'interet
    k_list = [] #liste totale initialement vide
    # On cre les coordonnees sur les segments du trajet avec des arrays reguliers
    liste_kx1 = np.linspace(Gammax, Kx, Nk1, endpoint= False)
    liste_ky1 = np.linspace(Gammay, Ky, Nk1, endpoint= False)
    liste_kx2 = np.linspace(Kx, Mx, Nk2, endpoint= False)
    liste_ky2 = np.linspace(Ky, My, Nk2, endpoint= False)
    liste_kx3 = np.linspace(Mx, Gammax, Nk3, endpoint= False)
    liste_ky3 = np.linspace(My, Gammay, Nk3, endpoint= False)
    # on combine les listes de coordonnees
    liste_kx = np.hstack((liste_kx1,liste_kx2,liste_kx3))
    liste_ky = np.hstack((liste_ky1,liste_ky2,liste_ky3))
    # On utilise les listes de coordonnees pour creer les vecteurs d'ondes sur le trajet
    for i in range(len(liste_kx)):
        kx = liste_kx[i]
        ky = liste_ky[i]
        k = np.array([kx, ky,0.0])
        k_list.append(k)
    return k_list, Nk_list
    
def create_k_list_GammaKMGamma_IP(Nk, maille_param): # pour reseau 'hexagonal' champ In plane
    """ Creation d'un trajet de vecteurs d'onde k dans l'espace reciproque. Le
     trajet passe par les points de hautes symetrie Gamma,K,M,Gamma
     du reseau reciproque d'un cristal de structure hexagonale selon le plan
     z-y (In plane). La fonction prend en entree le nombre approximatif de
     k a utiliser (arrondi vers le bas) et les parametres de maille du cristal.
     Elle retourne un array conteannt les k du trajet et une liste contenant
     les indices de la liste ou se trouve chaque points de hautes
     symetrie dans le array precedent, dans ordre indique ci-haut. """     
    #definition des pts d'interet    
    a = maille_param[0] # parametre de maille
    Gammaz = 0.001 #eviter les erreurs numeriques lors de la PWM, In plane
    Gammay = 0.001
    Kz = np.pi/a
    Ky = 0.0
    Mz = (np.sqrt(3.0)*np.pi)/(2.0*a)
    My = np.pi/(2.0*a)
    L_trajet_total = (Kz - Gammaz) + np.sqrt((Mz - Kz)**2 +(My - Ky)**2) + np.sqrt((Gammaz - Mz)**2 +(Gammay - My)**2)
    Nk1 = np.floor(((Kz - Gammaz)/L_trajet_total)*Nk) # proportion du trajet, en points, entre Gamma et K
    Nk2 = np.floor((np.sqrt((Mz - Kz)**2 +(My - Ky)**2)/L_trajet_total)*Nk) # proportion du trajet, en points, entre K et M
    Nk3 = np.floor((np.sqrt((Gammaz - Mz)**2 +(Gammay - My)**2)/L_trajet_total)*Nk) # proportion du trajet, en points, entre K et M
    Nk_list = [Nk1, Nk1+Nk2, Nk1+Nk2+Nk3]  #nombre total de vecteur rellement crees    
    # On cre une liste en se deplacant regulierement entre les pts d'interet
    k_list = [] #liste totale initialement vide
    # On cre les coordonnees sur les segments du trajet avec des arrays reguliers
    liste_kz1 = np.linspace(Gammaz, Kz, Nk1, endpoint= False)
    liste_ky1 = np.linspace(Gammay, Ky, Nk1, endpoint= False)
    liste_kz2 = np.linspace(Kz, Mz, Nk2, endpoint= False)
    liste_ky2 = np.linspace(Ky, My, Nk2, endpoint= False)
    liste_kz3 = np.linspace(Mz, Gammaz, Nk3, endpoint= False)
    liste_ky3 = np.linspace(My, Gammay, Nk3, endpoint= False)
    # on combine les listes de coordonnees
    liste_kz = np.hstack((liste_kz1,liste_kz2,liste_kz3))
    liste_ky = np.hstack((liste_ky1,liste_ky2,liste_ky3))
    # On utilise les listes de coordonnees pour creer les vecteurs d'ondes sur le trajet
    for i in range(len(liste_kz)):
        kz = liste_kz[i]
        ky = liste_ky[i]
        k = np.array([0.0, ky,kz])
        k_list.append(k)
    return k_list, Nk_list
