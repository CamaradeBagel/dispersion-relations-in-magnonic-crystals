#!/bin/bash
#PBS -l walltime=50:00:00
#PBS -l nodes=4:ppn=12
#PBS -q normale
#PBS -r n

DOSSIER="magnonic_crystal"
PYSCRIPT="PWM_test1.py"
CUSTOMMODULE="CristauxMagnoniques.py"

# Se deplacer dans le dossier
cd $HOME/$DOSSIER
# Copier le fichier dans le SCRATCH
cp $PYSCRIPT $SCRATCH
cp $CUSTOMMODULE $SCRATCH
# Se deplacer dans le $SCRATCH
cd $SCRATCH

# Creer d'un environnement python virtuel
module load python/3.5.1
pyvenv environnementPython
# Activativer de l'environnement virtuel
source environnementPython/bin/activate
# (On a deja installe numpy et scipy)

# Demander l'execution du script python
python3 $SCRATCH/$PYSCRIPT
# Deplacer les resultats dans dossier
mv $SCRATCH/*.gz $HOME/$DOSSIER
# Supprimer le script python du SCRATCH
rm $SCRATCH/$PYSCRIPT
rm $SCRATCH/$CUSTOMMODULE

# Desactiver l'environnement virtuel
deactivate
