# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 14:38:10 2018

Calculs de relations de dispersion pour un cristal magnonique selon la
 decomposition en ondes planes (PWM.)
- On considere des inclusions de nickel , base sur les reseaux de rectangle 
  fabriques par Alexis Boucher pour le laboratoire de magnetisme
- On considère un champ H0 externe de saturation appliqué Out of plane.
- Les calculs sont effectues de maniere parallelle sur le nombre de coeur
 disponibles.
 
 Objectif: effectuer les calculs sur les serveurs de Calcul Quebec

@author: Bastien GauthierSoumis
"""

from CristauxMagnoniques import *
from multiprocessing import Pool
from  datetime import datetime

# Definition de la precision sauf nb d'ondes planes
Nk = 60 # nombre de vecteurs d'onde du trajet(arrondi vers le bas à un moment)
# Definition du champ applique externe et uniforme
H0 = (1)/mu0

"""definition d'un reseau hexagonal, inclusions cylindres, H0 Out of Plane"""
# Definition des parametres du reseau
t = 20e-9 #[m], epaisseur de la couche en z #!
az = ay = 400e-9 #[m], parametre de maille 
maille_param = [az, ay, t]
# Definition des parametres des inclusion
Ms_inclusion = 0.484e6# [A/m] Magnetisation a saturation des inclusions (Ni)
Ms_matrice = 1.752e6# [A/m] Magnetisation a saturation de la matrice (Fe)
Ms_r = [Ms_inclusion, Ms_matrice]
lex2_inclusion = (7.64e-9)**2 # [m**2] echange des inclusions (Ni)
lex2_matrice = (3.30e-9)**2 # [m**2] echange de la matrice (Fe)
lex2_r = [lex2_inclusion, lex2_matrice]
inclusion_type = 'rectangle'
lz, ly = np.sqrt(0.55)*az, np.sqrt(0.55)*ay # [m], dimension des inclusions
inclusion_param = [lz, ly]
inclusion = [inclusion_type, inclusion_param]
""" Utilisation des fonctions et classes definies dans CristauxMagnonique.py"""
# On defini le reseau reciproque avec differents rangs de voisins
nG_list = [5,6] # nombre de rangees de voisins consideres dans
                        # le reseau reciproque
# Pour chaque nG, on definit un reseau et un cristal magnonique
MagCryst_rectangle_IP_list = []
for nG in nG_list:
    G_data_rectangle_IP = create_rectangular_lattice_reciproqual_IP(nG, az, ay, t)
    # On defini les cristaux magnoniques
    MagCryst_rectangle_IP_list.append( MagnonicCrystal(G_data_rectangle_IP, Ms_r, lex2_r,\
                                         inclusion))

# On defini le chemin sur lequel on etudie les relations de dispersion
k_list_rectangle_IP, Nk_list_rectangle_IP = create_k_list_GammaXMGammaX1M_IP(Nk,\
                                                                maille_param)



"""Calcul parallelise des relations de dispersion"""
for MagCryst_rectangle_IP in MagCryst_rectangle_IP_list:
    w_all_array_rectangle_IP, info_rectangle_IP = ParaCompute(MagCryst_rectangle_IP, k_list_rectangle_IP, Nk_list_rectangle_IP, H0)